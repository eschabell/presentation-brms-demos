Red Hat JBoss BRMS Demo Projects
--------------------------------
View presentation here: [https://eschabell.gitlab.io/presentation-brms-demos](https://eschabell.gitlab.io/presentation-brms-demos)


![Cover Slide](cover.png)


Released versions
-----------------

- v1.0 - session delivered on Dec 17, 2015.
